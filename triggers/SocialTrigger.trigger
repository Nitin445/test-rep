/*This trigger is for creating attachment with the images from the social post*/
 
trigger SocialTrigger on SocialPost (after insert) {

  if(Trigger.new != null)
  {
      for(SocialPost oSocialPost: Trigger.new)  
      {  
        if(oSocialPost != null && String.isNotBlank(oSocialPost.AttachmentUrl))
        {    
           SocialAttachmentHandler.createAttachment(oSocialPost.AttachmentUrl, oSocialPost.Id); 
        } 
      }

  }
}