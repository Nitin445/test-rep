<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>REYER</fullName>
        <field>Comments_Workflow__c</field>
        <formula>Description</formula>
        <name>REYER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Call_Option_From_Call_Type_Outbou</fullName>
        <description>Updates ININ calls from standard call type field to call option as outbound</description>
        <field>Call_Option__c</field>
        <literalValue>Outbound</literalValue>
        <name>Update Call Option From Call Type Outbou</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Call_Option_from_Call_Type_Stand</fullName>
        <field>Call_Option__c</field>
        <literalValue>Inbound</literalValue>
        <name>Update Call Option from Call Type(Stand)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SMS_Status</fullName>
        <description>Update SMS Status</description>
        <field>SMS_Send_Status__c</field>
        <literalValue>Sent</literalValue>
        <name>Update SMS Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SMS_Task_Completed</fullName>
        <description>Update SMS task to Completed</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Update SMS task Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_RecordType</fullName>
        <description>If type of the task is email then change record type to Email.</description>
        <field>RecordTypeId</field>
        <lookupValue>Email</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Task RecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_SMS</fullName>
        <apiVersion>36.0</apiVersion>
        <endpointUrl>https://www.belong.com.au/api/services/salesforce/task/sms/notification</endpointUrl>
        <fields>Description</fields>
        <fields>Id</fields>
        <fields>SMS_Recipient__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Send SMS</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Send SMS</fullName>
        <actions>
            <name>Update_SMS_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SMS_Task_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_SMS</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>SMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.SMS_Send_Status__c</field>
            <operation>equals</operation>
            <value>Send</value>
        </criteriaItems>
        <description>Send SMS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task %3A Update Comments for displayig in Related list</fullName>
        <actions>
            <name>REYER</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Description )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Call Option From Call Type</fullName>
        <actions>
            <name>Update_Call_Option_from_Call_Type_Stand</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.CallType</field>
            <operation>equals</operation>
            <value>Inbound</value>
        </criteriaItems>
        <description>Update call option from call type field which gets updated from ININ.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Call Option From Call Type - Outbound</fullName>
        <actions>
            <name>Update_Call_Option_From_Call_Type_Outbou</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.CallType</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <description>Update call option from call type field which gets updated from ININ.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Task RecordType</fullName>
        <actions>
            <name>Update_Task_RecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
