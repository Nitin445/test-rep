<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Cooling_Off_Customer_TRUE</fullName>
        <description>Cooling Off Customer = True</description>
        <field>Cooling_Off_Customer__c</field>
        <literalValue>1</literalValue>
        <name>Cooling Off Customer TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cooling Off Customer</fullName>
        <actions>
            <name>Cooling_Off_Customer_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Order_Step__c</field>
            <operation>equals</operation>
            <value>STEP_COOLING_OFF</value>
        </criteriaItems>
        <description>Workflow checks if Order Step = STEP_COOLING_OFF</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
