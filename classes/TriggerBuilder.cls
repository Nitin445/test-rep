/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Generic class for Triggers
 
*/
public with sharing class TriggerBuilder 
{
    public static void handle(type triggerType)
    {   
        TriggerInterface handler = getHandler(triggerType);
        if(handler==null)
        {
            throw new TriggerException('Invalid trigger handler');
        }
        executeHandler(handler);
    }

    private static void executeHandler(TriggerInterface handler)
    {
        System.debug(LoggingLevel.ERROR,'====>TriggerBuilder.executeHandler is called:' + handler);
        
        if(Trigger.isBefore) //Handling before context
        {
            handler.cacheBefore();
            
            if (Trigger.isDelete)
            {
                for (sObject oldSObj : Trigger.old)
                {
                    handler.beforeDelete(oldSObj);
                }
            }
            else if (Trigger.isInsert)
            {
                for (sObject newSObj : Trigger.new)
                {
                    handler.beforeInsert(newSObj);
                }
            }
            else if (Trigger.isUpdate)
            {
                for (sObject newSObj : Trigger.new)
                {
                    handler.beforeUpdate(Trigger.oldMap.get(newSObj.id), newSObj);
                }
            }                   
        }
        else //Handling after context
        {
            handler.cacheAfter();

            if (Trigger.isUndelete)
            {
                for (sObject newSObj : Trigger.new)
                {
                    handler.afterUndelete(newSObj);
                }
            }           
            if (Trigger.isDelete)
            {
                for (sObject oldSObj : Trigger.old)
                {
                    handler.afterDelete(oldSObj);
                }
            }
            else if (Trigger.isInsert)
            {
                for (sObject newSObj : Trigger.new)
                {
                    handler.afterInsert(newSObj);
                }
            }
            else if (Trigger.isUpdate)
            {
                for (sObject newSObj : Trigger.new)
                {
                    handler.afterUpdate(Trigger.oldMap.get(newSObj.id), newSObj);
                }
            }       
            handler.afterProcessing();
        }
        //handler.afterProcessing();
    }

    private static TriggerInterface getHandler(Type objectType)
    {
        Object oBj = objectType.newInstance();
        TriggerInterface returnObj = null;
        
        if (oBj instanceOf TriggerInterface)
        {
           returnObj = (TriggerInterface) oBj;
        }
 
        return returnObj ;
    }
    
    public class TriggerException extends Exception {}
}