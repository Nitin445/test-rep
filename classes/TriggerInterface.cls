/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Trigger Interface to be implemented by trigger handlers
*/
public interface TriggerInterface 
{
	//Caches. Soql should be implemented in these method and made available via maps
	//Cache related data for before triggers, called once on start of the trigger execution
	void cacheBefore();
	//Cache related data for after triggers, called once on start of the trigger execution
	void cacheAfter();

	//process row per context, soql and DML MUST NOT be implemeted in any of the context methods
	//There may be called multiple times are it's per row

	//Process a row on beforeInsert
	void beforeInsert(sObject newSObj);

	//Process a row on afterInsert
	void afterInsert(sObject newSObj);

	//Process a row on beforeUpdate
	void beforeUpdate(sObject oldSObj, sObject newSObj);

	//Process a row on afterUpdate
	void afterUpdate(sObject oldSObj, sObject newSObj);

	//Process a row on beforeDelete
	void beforeDelete(sObject oldSObj);

	//Process a row on afterDelete
	void afterDelete(sObject oldSObj);

	//Process a row on afterUndelete
	void afterUndelete(sObject newSObj);


	//Ater all rows have been processed this method will be called
	//Usualy DML are placed in this method
	void afterProcessing();
}