/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : util class for trigger handlers and others
 */
public with sharing class GlobalUtil 
{
    private static Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe();
    private static Map<String, Event_Matrix__C> eventMatrixGlobalMap;
    private static Set<String> excludedFields = new Set<String>{
    'email_template_code__c',
    'sms_template_code__c',
    'notification_type__c',
    'key_name__c'    
    };
    
    private static List<Holiday> holidays 
    {
        get 
        {
            if (holidays == null) 
            {
                holidays=[SELECT StartTimeInMinutes, Name, ActivityDate FROM Holiday ];
            }
            return holidays;
        }
        private set;
    }
    
    public static Map<String,Schema.RecordTypeInfo> getRecordTypeByName(String sObjectName)
    {
        Schema.SObjectType s;
        Schema.DescribeSObjectResult resSchema;
        
        s           = sObjectMap.get(sObjectName) ; // getting Sobject Type 
        resSchema   = s.getDescribe() ;
        
        return resSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
    }
    
    public static Map<Id,Schema.RecordTypeInfo> getRecordTypeById(String sObjectName)
    {
        Schema.SObjectType s;
        Schema.DescribeSObjectResult resSchema;
        
        s           = sObjectMap.get(sObjectName) ; // getting Sobject Type
        resSchema   = s.getDescribe() ;
        
        return resSchema.getRecordTypeInfosById(); //getting all Recordtype for the Sobject
    }
    
    // METHOD: addBusinessDays
    // PURPOSE: increments a date by numDays, while not counting weekend days
    public static Date addBusinessDays(Date theDate, integer numDays)
    {
        Set <String> weekendDays = new Set <String> {'Saturday', 'Sunday'};
        DateTime addedToDate = DateTime.newInstance (theDate, Time.newInstance(0, 0, 0, 0));
        
        // Loop numDays times and add days to the date. Skip over weekend days.
        for(integer i=0; i < numDays; i++)
        {
            addedToDate = addedToDate.addDays(1);
            while (weekendDays.contains(addedToDate.format('EEEEE')))
            {
                addedToDate = addedToDate.addDays(1);
            }
        }

        return addedToDate.date();
    }
    
    public static DateTime AddBusinessDays(DateTime StartDate, integer BusinessDaysToAdd)
    {
        //Add or decrease in BusinessDaysToAdd days 
        DateTime finalDate = StartDate;
        integer direction = BusinessDaysToAdd < 0 ? -1 : 1;
        system.debug('direction = '+direction);
        while(BusinessDaysToAdd != 0)
        {
            finalDate = finalDate.AddDays(direction);
            if (checkifWorkDay(finalDate.date()))
            {
                BusinessDaysToAdd -= direction;
            }
        }
        return finalDate;
    }
    
    //To check if sent date is a working day by comparing with org holidays
    public static boolean checkifWorkDay(Date sentDate)
    {
        Date weekStart  = sentDate.toStartofWeek();
        for(Holiday hday:holidays)
        {
            if(sentDate.daysBetween(hday.ActivityDate) == 0)                        
                return false;                        
        }
                
        Datetime dt = DateTime.newInstance(sentDate, Time.newInstance(0, 0, 0, 0));
        if(dt.format('EEEE') == 'Saturday' || dt.format('EEEE') == 'Sunday')
            return false;
        else 
            return true;
    }
    
    private static Map<String, Schema.SObjectType> mapSObjectNameSObjType = Schema.getGlobalDescribe();
    
    public static String mergeSMSTemplateWithSObject(String templateContent, String sobjectName, String recordId) 
    {
        // Get all fields' description of the sboject
        Map<String, Schema.SObjectField> mapSObjectFields = mapSObjectNameSObjType.get(sobjectName).getDescribe().fields.getMap();

        // Find all sobject fields' placeholder in the template content. i.e sObject.fieldName.
        String fieldPlaceHolderRegex = '((?i)\\{' + sobjectName + '\\.[^\\}]+\\})';
        Pattern patrn = Pattern.compile(fieldPlaceHolderRegex);
        Matcher matcr = patrn.matcher(templateContent);
        Set<String> set_placeHolders = new Set<String>(); 

        String queryString = 'SELECT ';
        Set<String> set_validFieldName = new set<String>(); // keep track of fields in query
        while (matcr.find()) 
        {
            String fieldFind = templateContent.subString(matcr.start() + 1, matcr.end() - 1); // REMOVE THE {}
            String fieldName = fieldFind.split('\\.').get(1).toLowerCase();

            // Found one place holder
            set_placeHolders.add(fieldFind);

            if (!set_validFieldName.contains(fieldName) && mapSObjectFields.containsKey(fieldName)) 
            {
                // the field name is a valid field of this sobject, add it to valid field name set and query string
                queryString += fieldName + ', ';
                set_validFieldName.add(fieldName);
            }
        }
        // Query sobject record and merge with the template
        if (!set_placeHolders.isEmpty()) 
        {
            queryString  = queryString.subString(0, queryString.length() - 2);
            queryString += ' FROM ' + sobjectName + ' WHERE id =: recordId';
            for (Sobject record : database.query(queryString))
            {
                // Merge all placeholders with the template
                for (String key : set_placeHolders) 
                {
                    String fieldName = key.split('\\.').get(1).toLowerCase();

                    // Get field value
                    String placeValue = '';
                    if (set_validFieldName.contains(fieldName)) 
                    {
                        Schema.DisplayType fieldType = mapSObjectFields.get(fieldName).getDescribe().getType();
                        Object fieldValue = record.get(fieldName);
                        if (fieldValue == null) 
                        {
                            placeValue = '';
                        } 
                        else if (fieldType == Schema.DisplayType.Date) 
                        {
                            placeValue = date.valueOf(fieldValue).format();
                        } 
                        else if (fieldType == Schema.DisplayType.DateTime) 
                        {
                            placeValue = datetime.valueOf(fieldValue).format();
                        } 
                        else 
                        {
                            placeValue = fieldValue + '';
                        }
                    }                   
                    // Replace value with placeholders
                    templateContent = templateContent.replace('{' + key + '}', placeValue);
                }
            }
        }

        return templateContent;
    }
    
    //merge SMS template with a set of Values
    public static String mergeSMSTemplateWithFields(SMS_Template__c smsTemplate, Map<String, String> fieldValueMap)
    {
        // Find all fields' placeholder in the template content. i.e FieldMap Values.
        String templateContent = smsTemplate.SMS_Template__c;
        for(String keyValue :  fieldValueMap.keySet())
        {
            String fieldValue = fieldValueMap.get(keyValue);
            keyValue = '{' + keyValue + '}';
            
            templateContent = templateContent.replace(keyValue, fieldValue);
        }
        return templateContent;
    }
    
    //Convert a list of Formaty VALUE;VALUE into as map
    public static Map<String,String> getHashMap(List<String> fieldValuePair)
    {
        Map<String, String> fieldValueMap = new Map<String, String>();

        for(String fieldValue : fieldValuePair)
        {
            //List<String> strArr = fieldValue.split(':');
            fieldValueMap.put(fieldValue.substringBefore(':'), fieldValue.substringAfter(':'));
        }

        return fieldValueMap;
    }
    
    public static String encrypt(String valueToEncrypt, Integer keyLength, Blob cryptoKey){ 
        // Generate the data to be encrypted.
        Blob data = Blob.valueof(valueToEncrypt);

        // Encrypt the data and have Salesforce.com generate the initialization vector
        Blob encryptedData = Crypto.encryptWithManagedIV('AES'+String.valueOf(keyLength), cryptoKey, data);     
        
        return EncodingUtil.convertToHex(encryptedData);                        
    }
    
    public static String decrypt(String valueToDecrypt, Integer keyLength, Blob cryptoKey){ 
        //Decrypt the data - the first 16 bytes contain the initialization vector
        Blob encryptedData=Encodingutil.convertFromHex(valueToDecrypt);
        
        Blob decryptedData = Crypto.decryptWithManagedIV('AES'+String.valueOf(keyLength), cryptoKey, encryptedData);
        
        return decryptedData.toString();                        
    }
    
    //Get Tamplate code and notification type from EVENY_MATRIX custom setting
    //based on inputs - Event type,subtype and others
    /*public static Event_Matrix__C getTemplateDetails2(List<String> eventAttributes)
    {
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Event_Matrix__C.fields.getMap();
        Set<String> requiredFields = new Set<String>();

        for(String keyname : fieldMap.keySet())
        {
            if(fieldMap.get(keyname).getDescribe().isCustom() && !excludedFields.contains(keyname))
            {
                requiredFields.add(keyname);
            }            
        }

        Map<String,String> eventmatrixMap = getHashMap(eventAttributes);

        //System.debug('eventmatrixMap: '+eventmatrixMap);

        String query = 'select Email_Template_Code__c, SMS_Template_Code__c, notification_type__c from Event_Matrix__C where ';

        for(String field : requiredFields)
        {
            //System.debug('field: '+ field);
            if(String.isNotBlank(eventmatrixMap.get(field.replace('__c',''))))    
            {
                query += field + ' = ' + '\'' + eventmatrixMap.get(field.replace('__c',''))+ '\' and ';
            }
            else
            {
                query += field + ' = ' + '\'\' and ';
            }
        
        }

        System.debug('query: ' + query);

        List<Event_Matrix__C> eventMatrixList = Database.query(query.removeEnd(' and '));
        Event_Matrix__C eventMatrix;

        if(eventMatrixList.size() > 0)
        {
            eventMatrix = eventMatrixList[0];
        }else
        {
            eventMatrix = null;
        }

        return eventMatrix;

    }*/ 

    public static Event_Matrix__C getTemplateDetails(List<String> eventAttributes)
    {
        
        if(eventMatrixGlobalMap == null)
        {
            eventMatrixGlobalMap = GlobalUtil.getAllEventMatrixMap();     
        }
        

        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Event_Matrix__C.fields.getMap();
        List<String> requiredFields = new List<String>();
        Event_Matrix__C eventMatricObj = null;

        for(String keyname : fieldMap.keySet())
        {
            if(fieldMap.get(keyname).getDescribe().isCustom() && !excludedFields.contains(keyname))
            {
                requiredFields.add(keyname);
            }            
        }
        requiredFields.sort();

        String keyName = '';

        Map<String,String> eventAttrMap = getHashMap(eventAttributes);

        for(String field : requiredFields)
        {            
            if(String.isNotBlank(eventAttrMap.get(field.replace('__c',''))))    
            {
                keyName += eventAttrMap.get(field.replace('__c','')) + ':';
            }
        
        }

        System.debug('KeyName: '+ keyName.removeEnd(':'));

        if(String.isNotBlank(keyName))
        {
            eventMatricObj = eventMatrixGlobalMap.get(keyName.removeEnd(':'));
        }

        return eventMatricObj;

    }

    public static Map<String, Event_Matrix__C> getAllEventMatrixMap()
    {
        Map<String, Event_Matrix__C> eventMatrixMap = new Map<String, Event_Matrix__C>();

        Map<String, Event_Matrix__C> customSettings = Event_Matrix__C.getAll();

        for(Event_Matrix__C customSetting : customSettings.values())
        {
            eventMatrixMap.put(customSetting.key_name__c,customSetting);
        }

        return eventMatrixMap;
    }

    public static Task createSMSTask(Account accountObj, String subjectName, boolean isBillingFlag, String descriptionText)
    {
        Task taskSMS = new Task();
        taskSMS.WhatId = accountObj.Id;
        taskSMS.status = 'Completed';
        taskSMS.SMS_Send_Status__c = 'Send';
        taskSMS.Subject = subjectName;
        Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Task');
        taskSMS.RecordTypeId    = taskRecordTypeInfoName.get('SMS').getrecordTypeId();            
        taskSMS.Billing__c = isBillingFlag;

        for(Contact conObj : accountObj.Contacts)
        {
            if(conObj.Contact_Role__c == 'Primary')
            {
                taskSMS.WhoId = conObj.Id;
                taskSMS.SMS_Recipient__c = String.valueof(conObj.MobilePhone);
                break;
            }
        }
        taskSMS.Description = descriptionText;

        return taskSMS;
    }

    public static Log__c createLog(String level, String message, String methodName, String request)
    {
        Log__c logObj = new Log__c(Level__c = level, Message__c = message, Method_Name__c = methodName, Request__c = request);
        return logObj;
    }

    public static String validateNotificationRequest(SFWrapperObject.Notification notifObj)
    {
        if(String.isNotBlank(notifObj.octaneNumber) && notifObj.eventAttributes != null && !notifObj.eventAttributes.isEmpty())
        {
            return null;
        }
        else
        {
            if(String.isBlank(notifObj.octaneNumber))
            {
                return 'No Octane Customer number in request';
            }
            else
            {
                return notifObj.octaneNumber + ' - Event attributes are null or Empty';
            }
        }
    }    
    
    public class UndefinedSObjectTypeException extends Exception {}
    
    public static AuthProperties__c getAuthPropertiesByName(String sName)
    {
        return [SELECT Name, Client_Id__c, Client_Secret__c, Password__c, Username__c, Endpoint__c FROM AuthProperties__c WHERE Name = :sName ];            
    }
    
}