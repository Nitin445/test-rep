public with sharing class SequenceFAQWeightedScore {
    
    public List<FAQ__kav> allRecords {get;set;}
    public List<FAQ__kav> oldallRecords = new List<FAQ__kav>();
    
    Public set<String> product_header_names{get;set;}
    
    Public map<String,String> selected_product_title {get;set;}
    Public map<Id, FAQ__kav> mapOldFAQ;
    
    Public string selected_product_title_GenerateFAQ {get;set;}
    public string prdtHeading {get;set;}
    Public string selectedPrdtTitle {get;set;}
    
    Public boolean boolpbfaq {get;set;}
    Public boolean boolSelectedPrdtTitle {get;set;}
    public boolean errorMessage{get;set;}
    
    
    Public SequenceFAQWeightedScore (){
		boolpbfaq = False;
        product_header_names= new Set<String>(); 
        for(Public_Knowledge_Product__c Header : [select id, Product_Category__c, title__c, Data_Category__c from Public_Knowledge_Product__c]){
            Product_Header_Names.add(header.Product_Category__c.touppercase());
        }
    }
     
    public PageReference SelectedProductTitle() {
        boolSelectedPrdtTitle = true;
        boolpbfaq = False;
        selected_product_title = new Map<String,String>(); 
        for(Public_Knowledge_Product__c Title : [select id, Title__c, Data_Category__c from Public_Knowledge_Product__c where Product_Category__c = :prdtHeading]){
            selected_product_title.put(Title.Title__c, Title.Data_Category__c);
        }
        return null;
    }
    
    public Pagereference getRecords() {
        boolpbfaq = True;
        rerender_display_data();        
        return null;
    }
    
    public Pagereference SaveReorderSequence() {
        
        if(boolpbfaq==false){
            errorMessage = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Before Saving, enter the Weighted Scores'));
            return null;
        }
        errorMessage = false;
        List<faq__kav> updObjList = new List<faq__kav>();
        Integer score = 0;
        Map<String,Integer> updIdToScoreMap = new Map<String,Integer>();
        faq__kav editdobj;
            for(faq__kav origobj : allRecords){
                editdobj = mapOldFAQ.get(origobj.Id);
                if(editdobj.Weighted_Score__c != origobj.Weighted_Score__c){
                    updIdToScoreMap.put(String.valueOf(origobj.id),Integer.valueof(origobj.Weighted_Score__c));
                    score = Integer.valueof(origobj.Weighted_Score__c);
                    
                    if((score != NULL && !string.valueof(score).isnumeric()) || (score != NULL && integer.valueof(score) == 0)){
                        origobj.Weighted_Score__c = NULL;
                        errorMessage = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Enter numeric Value greater than 0'));        
                        return null;
                    }
                    if(updObjList.size() > 1){
                        errorMessage = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Error: More than 1 record selected'));
                        return null;
                    }
                    updObjList.add(origobj);
                }
            }
        
        if(score != NULL){
            for(faq__kav obj : mapOldFAQ.values()){
                if(!updIdToScoreMap.containsKey(String.valueOf(obj.Id))) {
                    if(Integer.valueOf(obj.Weighted_Score__c) == score){
                        obj.Weighted_Score__c = Integer.valueOf(obj.Weighted_Score__c)+1;    
                        score++;
                        updObjList.add(obj);
                    }                
                }
            }
        }
        if(updObjList != null && updObjList.size() > 0){            
            for(faq__kav obj : updObjList){
                Integer GetScore = Obj.Weighted_Score__c != NULL ? Integer.valueof(obj.Weighted_Score__c) : NULL;
                updateArticles('FAQ', GetScore , String.valueof(obj.ArticleNumber)); 
            }
        }
		rerender_display_data();
        return null;
    }
    
    public PageReference updateArticles(String articleType, integer WeighScore, string Artnumbr) {
        List<Id> articleIDs = new List<Id>();
        List<String> articleNumbers = new List<String>();

        String query = 'SELECT Id, ArticleNumber, KnowledgeArticleId FROM '  + articleType+ '__kav WHERE PublishStatus = \'online\' and language = \'en_US\' and ArticleNumber = \'' + Artnumbr + '\' limit 100';
        for (sObject obj : Database.query(query)) {
            String Id = KbManagement.PublishingService.editOnlineArticle(String.valueOf(obj.get('KnowledgeArticleId')), false);
            if (Id == null) {System.debug('##### ERROR EDITING');}
            articleNumbers.add(String.valueOf(obj.get('ArticleNumber')));
        }
		List<sObject> articles = new List<sObject>();
        query = 'SELECT Id, KnowledgeArticleId FROM ' + articleType + '__kav WHERE PublishStatus = \'draft\' AND Language = \'en_US\' AND ArticleNumber IN :articleNumbers limit 100';
        for (sObject obj : Database.query(query)) {
            obj.put('IsVisibleInPkb',true);
            obj.put('Weighted_Score__c',WeighScore);
            articleIDs.add(String.valueOf(obj.get('KnowledgeArticleId')));
            articles.add(obj);
            
        }
        update articles;
        for (String articleId : articleIds) {
            KbManagement.PublishingService.publishArticle(articleId, true);
        }
        return null;
    }
    
    public pagereference rerender_display_data (){
        string Query = 'select id, ArticleNumber, Question__c, VersionNumber, Weighted_Score__c FROM FAQ__kav WHERE Language = \'en_US\' AND PublishStatus = \'Online\' WITH DATA CATEGORY Category__c at ';
        Query += selected_product_title.get(selectedPrdtTitle) + ' ORDER by Weighted_Score__c nulls last' ;
        mapOldFAQ = new Map<Id, FAQ__kav>((List<FAQ__kav>)Database.query(query));
        allrecords = ((List<FAQ__kav>)mapOldFAQ.values()).deepclone(true,false,true);
        return null;
    }
    
    Public PageReference cancel() {
        PageReference homepage = new pagereference('/');
        return homepage;
    }
}