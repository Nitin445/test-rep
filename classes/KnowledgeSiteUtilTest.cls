@isTest
private class KnowledgeSiteUtilTest
{
	@testSetup static void setupTestData() 
    {
        TestUtil.createKnowledgeSettings();
    }

	static testmethod void testJoinList()
    {
    	List<String> stringList = new List<String>();
    	stringList.add('1234');
    	stringList.add('4556');
    	KnowledgeSiteUtil.join(stringList, ';');

    }

    static testmethod void testJoinEmptyList()
    {
    	List<String> stringList = new List<String>();
    	KnowledgeSiteUtil.join(stringList, ';');

    }

    static testmethod void testJoinBlankDelimeter()
    {
    	List<String> stringList = new List<String>();
    	stringList.add('1234');
    	stringList.add('4556');
    	KnowledgeSiteUtil.join(stringList, null);

    }

    static testmethod void testJoinSet()
    {
    	Set<String> stringSet = new Set<String>();
    	stringSet.add('1234');
    	stringSet.add('4556');
    	KnowledgeSiteUtil.join(stringSet, ';');
    }

    static testmethod void testBuildURLMapping()
    {
    	KnowledgeSiteUtil.buildURLMapping();
    }

    static testmethod void testBuildProductDisplay()
    {
    	KnowledgeSiteUtil.buildProductDisplay();
    }
}