/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  			Sathish Boopathy (SathishBabu.Boopathy@team.telstra.com)
  @created : 27/05/2016
  @Description : Test Class for the MetaHeaderController.
*/
@isTest
public with sharing class BelongMetaHeaderControllerTest
{
	 @testSetup static void setupTestData() 
    {
    	FAQ__kav faq = new FAQ__kav();
    	faq.title = 'test title';
    	faq.URLNAME = 'testUrl';
    	faq.Language  = 'en_US';
    	insert faq;
    	
    	TestUtil.createKnowledgeSettings();
    	
    }
	static testmethod void testGetterSetters()
    {
        Test.startTest();
        BelongMetaHeaderController controller = new BelongMetaHeaderController();

		BelongSupportHomeController BlngCntrl = controller.BlngCntrl;
	    BelongSupportSearchResultsController BlngSrchCon = controller.BlngSrchCon;
	    BelongSupportArticleController BlngArtCntrl = controller.BlngArtCntrl;
	    BelongSupportCategoryController BlngCatCntrl = controller.BlngCatCntrl;
	    Test.stopTest();
	}

}