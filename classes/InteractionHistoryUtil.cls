public with sharing class InteractionHistoryUtil 
{
    private Static final Set<String> contentList = new Set<String> {'We haven\'t been able to debit', 
                                                                    'You currently have a charge of',
                                                                    'As we did not receive your monthly payment',
                                                                    'your Belong service will be re-activated',
                                                                    'Your Belong service has been cancelled as you haven\'t paid' };
    
  public class InteractionHistoryResponse
    {
        public integer totalSize;
        List<InteractionHistory> interactionHistoryList;

    }

  public class InteractionHistory
    {
        public String id;
        public String accountId;
        public String activityDate;
        public String type;
        public String description;        
    }

  public Static InteractionHistoryResponse retrieveActivities(String octaneCustomer, integer offsetNumber, 
                            integer size, List<String> filterTypes) 
    {
        InteractionHistoryResponse response = new InteractionHistoryResponse();
        response.totalSize = 0;

        if(filterTypes == null || filterTypes.isEmpty())
        {
            filterTypes = new List<String>();
            filterTypes.add('SMS');
            filterTypes.add('Email');
        }

        Account accountObjForCount = [ SELECT id, (SELECT  id
                                          			FROM Tasks 
                                          			WHERE RecordType.DeveloperName in :filterTypes AND 
                                                		Billing__c = false) 
                               			FROM Account 
                               			WHERE Octane_Customer_Number__c =: octaneCustomer LIMIT 1];

        if(accountObjForCount!=null && !accountObjForCount.Tasks.isEmpty())
        {
          response.totalSize = accountObjForCount.Tasks.size();
        }

        Account accountObj = [ SELECT id, (SELECT  id, activityDate, Notification_Date_Time__c, description, RecordType.DeveloperName, accountId 
                                          	FROM Tasks 
                                         	WHERE RecordType.DeveloperName in :filterTypes and 
                                                Billing__c = false
                                          		ORDER by activityDate DESC LIMIT :size OFFSET :offsetNumber) 
                               FROM Account 
                               WHERE Octane_Customer_Number__c =: octaneCustomer LIMIT 1];

        List<InteractionHistory> interactionList = new List<InteractionHistory>();
        InteractionHistory interactionHistoryObj = null;

        if(accountObj!=null && !accountObj.Tasks.isEmpty())
        {
          for(Task taskObj: accountObj.Tasks)
          {
            interactionHistoryObj = new InteractionHistory();
            interactionHistoryObj.accountId = taskObj.accountId;

            if(taskObj.Notification_Date_Time__c != null)
            {
                DateTime activityDateTime = taskObj.Notification_Date_Time__c;
                String dateInStringString = activityDateTime.format('dd/MM/yyyy hh:mm aaa');
                interactionHistoryObj.activityDate = dateInStringString;
            }

            String descriptionValue = taskObj.description;
            if(descriptionValue!=null && descriptionValue.contains('Body:'))
            {
                Integer indx = descriptionValue.indexOf('Body:');
                interactionHistoryObj.description = descriptionValue.substring(indx+6);               
            }
            else
            {
                interactionHistoryObj.description = descriptionValue;
            }
            interactionHistoryObj.id = taskObj.id;
            interactionHistoryObj.type = taskObj.RecordType.DeveloperName;
            interactionList.add(interactionHistoryObj);
          }
        }

        response.interactionHistoryList = interactionList;

        return response;
    }

    public Static void addInteraction(String utiliBillId, String type, String subject, String content,
                        String activityType, String activitySubType, String contactLogId, String contactLogUsername,
                        String email, String mobile, DateTime NotificationDateTime)
    {
        
        Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Task');

        Task taskObj = new Task();

        system.debug(logginglevel.error,'NotificationDateTime: '+ NotificationDateTime);

        Account accountObj = null;

        if (type.equalsIgnoreCase('SMS'))
        {
            taskObj.RecordTypeId    = taskRecordTypeInfoName.get('SMS').getrecordTypeId();
            
            accountObj = [SELECT Id, (SELECT Id 
										FROM Contacts 
                                        WHERE MobilePhone =: mobile)
							FROM  Account 
                            WHERE Octane_Customer_Number__c =: utiliBillId];

            if (accountObj.Contacts.size() > 0 && accountObj.Contacts[0] != null)
            {
                taskObj.WhoId = accountObj.Contacts[0].Id;
            }

            taskObj.Subject = type;
            taskObj.Description = content;
        }
        else
        {
            taskObj.RecordTypeId    = taskRecordTypeInfoName.get('Email').getrecordTypeId();

            accountObj = [SELECT Id, (SELECT Id 
                               			FROM Contacts 
                                        WHERE Email =: email)
							FROM  Account 
                            WHERE Octane_Customer_Number__c =: utiliBillId];

            if (accountObj.Contacts.size() > 0 && accountObj.Contacts[0] != null)
            {
                taskObj.WhoId = accountObj.Contacts[0].Id;
            }
            
            taskObj.Subject = 'EMAIL - ' + subject;
            taskObj.Description = content;
        }

        taskObj.Octane_ContactLog_ID__c = contactLogId;
        taskObj.Octane_ContactLog_Username__c = contactLogUsername;
        taskObj.WhatId = accountObj.Id;        
        
        if(NotificationDateTime != null)
        {
            Date activityDate = Date.newInstance(NotificationDateTime.yearGmt(), NotificationDateTime.monthGmt(), 
                NotificationDateTime.dayGmt());

            DateTime notificationTime = DateTime.newInstance(NotificationDateTime.yearGmt(), NotificationDateTime.monthGmt(), 
                NotificationDateTime.dayGmt(), NotificationDateTime.hourGmt(), NotificationDateTime.minuteGmt(), 
                NotificationDateTime.secondGmt());

            taskObj.ActivityDate = activityDate;
            taskObj.Notification_Date_Time__c = notificationTime;

            if(activitySubType == 'Activation' && activityType == 'Notification')
            {
                accountObj.Activation_date__c = activityDate;
                update accountObj;

            }
        }

        taskObj.Contact_Type__c = activityType;
        taskObj.Contact_Sub_Type__c = activitySubType;
        taskObj.Status = 'Completed';
        taskObj.SMS_Send_Status__c = 'Success';
        taskObj.SMS_Eve_Response__c = 'Integration from Octane';
        taskObj.Billing__c = updateBilling(content, activityType, activitySubType, type);

        insert taskObj;
    }

    private static boolean updateBilling(String content, String activityType, String activitySubType,
        String type)
    {

        system.debug(logginglevel.error,'contentList: '+ contentList);
        system.debug(logginglevel.error,'content: '+ content);

        if(type == 'SMS')
        {
            for(String fixedContent : contentList)
            {
                if(content.contains(fixedContent))
                {
                    return true;
                }
            }
        }
        
        return false;
    }
}