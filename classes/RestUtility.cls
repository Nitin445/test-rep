//This is class the utility class for REST service

public class RestUtility {

  //Http Request Object
  public Http createHttpRequest(){
        Http http = new Http();
        return http;
  }
  
  //Http Request Setup
  public HttpRequest createHttpRequest(String requestBody, String APICertificate, String endPointURL, String setMethord, Map<String, String> httpHeader){
        HttpRequest request = new HttpRequest();
        
        if(APICertificate!='')
        request.setClientCertificateName(APICertificate); 
                
        request.setEndpoint(endPointURL);
        
        request.setMethod(setMethord);  
         
        if(httpHeader.size()>0){  
            for(String htr : httpHeader.keySet()){
            request.setHeader(htr, httpHeader.get(htr));
            }
        }
        
        if(setMethord=='POST')
        request.setBody(requestBody);
        
        
        return request; 
  }
  
  //Http Request Response Setup
  public HttpResponse createHttpResponse(HttpRequest request,Http http ){
  
        HttpResponse response = http.send(request);
        return response;
  }
  

}