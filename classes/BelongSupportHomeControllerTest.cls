/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 27/05/2016
  @Description : Test Class for the BelongSupportCategoryController.
*/
@isTest
public with sharing class BelongSupportHomeControllerTest 
{
// test changed 23032017
@testSetup static void setupTestData() 
    {
    	FAQ__kav faq = new FAQ__kav();
    	faq.title = 'test title';
    	faq.URLNAME = 'testUrl';
    	faq.Language  = 'en_US';
    	insert faq;
    	
    	TestUtil.createKnowledgeSettings();
    }

    static testmethod void testBelongSupportHomeController()
    {
    	Test.startTest();
    	FAQ__kav faq = new FAQ__kav();
    	PageReference categoryPage = Page.Belong_Support_Home;
    	system.Test.setCurrentPage(categoryPage);
    	ApexPages.StandardController sc = new ApexPages.StandardController(faq);
    	BelongSupportHomeController controller = new BelongSupportHomeController(sc);
    	
    	String testStr;
    	
    	testStr = controller.pkbCon.pageTitle;
    	testStr = controller.currentSiteUrl;
    	testStr = controller.URLButton;
    	testStr = controller.siteName;
    	testStr = controller.currentSiteUrl;
    	testStr = controller.publishStatus;
    	testStr = controller.categorySelected;
    	testStr = controller.redirectURL;
    	boolean x = controller.isSite;
		
    	Test.stopTest();
    }
}