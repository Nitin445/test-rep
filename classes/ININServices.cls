/*
  @author  : Shrinivas Kalkote (shrinivas_kalkote@infosys.com)
  @created : 22/11/2016.
  @Description : This is ININ services class. All services which neds to be consumed from ININ are will be catered from this class. 
*/

public class ININServices{
public static final String CONST_EXTERNAL = 'External';
static String RELATIVE_PATH = '/oauth/token';
public static AuthProperties__c oAuthProperties = GlobalUtil.getAuthPropertiesByName('ININ_OAUTH');
public static AuthProperties__c oAuthProperties1 = GlobalUtil.getAuthPropertiesByName('ININ_API');
    
    public static ININConversationWrapper getININConversationDetails(String sConversationID, String OAUT_TOKEN)
    {
        RestUtility oRestUtility = new RestUtility();
        Map<String, String> mapRequestHeader = new Map<String, String>();
        
        if(String.isBlank(OAUT_TOKEN))
            OAUT_TOKEN = getOAuthToken();
    
        mapRequestHeader.put('Content-Type', 'application/json');
        mapRequestHeader.put('Authorization', 'Bearer ' + OAUT_TOKEN);
        
        String endPointURL = oAuthProperties1.EndPoint__c + '/api/v2/conversations/' + sConversationID;
        
        HttpRequest oResquest = oRestUtility.createHttpRequest('', '', endPointURL, 'GET', mapRequestHeader);
        HttpResponse oResponse = oRestUtility.createHttpResponse(oResquest, new Http()); 
        system.debug('oResponse:: ' + oResponse.getBody());
        
        ININConversationWrapper oININConversationWrapper;
        if(oResponse != NULL && oResponse.getStatusCode() == 200)
        {
            oININConversationWrapper = (ININConversationWrapper)JSON.deserialize(oResponse.getBody(), ININConversationWrapper.class);                    
            system.debug('oININConversationWrapper:: ' + oININConversationWrapper);
            for(ParticipantWrapper oParticipantWrapper: oININConversationWrapper.participants )
            {
                if(String.isNotBlank(oParticipantWrapper.participantType) && CONST_EXTERNAL.equalsIgnoreCase(oParticipantWrapper.participantType))
                {
                    oININConversationWrapper.participants[0] = oParticipantWrapper;
                    break;
                }
            }
            return oININConversationWrapper;
        }else{
            Log__c oLog = GlobalUtil.createLog('ERROR', 'Unable to get NPS data from ININ', 'getININConversationDetails()', oResponse.toString());
            oININConversationWrapper = new ININConversationWrapper();
            oININConversationWrapper.oLog = oLog;
        }
        return oININConversationWrapper;
    }
    
    public static String getOAuthToken()
    {
        
        RestUtility oRestUtility = new RestUtility();
        
        String requestStaticBody = 'grant_type=client_credentials';
        String endPointURL = oAuthProperties.EndPoint__c + RELATIVE_PATH;
        Map<String, String> mapRequestHeader = new Map<String, String>();
        
        String postCred = oAuthProperties.Client_Id__c + ':' + oAuthProperties.Client_Secret__c;
        String base64String = EncodingUtil.base64Encode(Blob.valueOf(postCred));
        String token;
        
        mapRequestHeader.put('Content-Type', 'application/x-www-form-urlencoded');
        mapRequestHeader.put('Authorization', 'Basic ' + base64String);
        
        HttpRequest oResquest = oRestUtility.createHttpRequest(requestStaticBody, '', endPointURL, 'POST', mapRequestHeader);
        HttpResponse oResponse = oRestUtility.createHttpResponse(oResquest, new Http()); 
        
        if(oResponse != null && oResponse.getStatusCode() == 200)
        {
            String sBody = oResponse.getBody();
            
            if(String.isNotBlank(sBody))
            {
                AuthClass oAuthClass = (AuthClass)JSON.deserialize(sBody, AuthClass.class);            
                token = oAuthClass.access_token;
                system.debug('OAUT_TOKEN :: ' + token );
                return token;
            }
            
        }else{
            //log the error
            Log__c oLog = GlobalUtil.createLog('ERROR', 'Authentication failed while contacting to ININ', 'getOAuthToken()', oResponse.toString());
            insert oLog;
            return null;
        }
        return null;
    }
    
    public class AuthClass{
        
        public String access_token{get; set;}
        public String expires_in{get; set;}
    }
    
    public class ININConversationWrapper{
        
        public String id{get; set;}
        public List<ParticipantWrapper> participants{get; set;}
        public Log__c oLog;
    }
    
    public class ParticipantWrapper{
        
        public Attributes attributes{get; set;}
        public String participantType{get; set;}
    }
    
    public class Attributes{
        public String NPS_Score{get; set;}
        public String NPS_Resolution{get; set;}
        public String NPS_Verbatim{get; set;}
    }
    
}