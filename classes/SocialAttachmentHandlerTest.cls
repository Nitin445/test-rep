@isTest
private class SocialAttachmentHandlerTest
{

  //This test method is to test the positive scenario , input:attachment URL of SocialPost , output: creats attachments in SocialPost   
  public static testmethod void testSocialAttachmentHandler1() {
  
     SingleRequestMock mockResponse = new SingleRequestMock(200,

                                                 'Complete',

                                                 '[{"Name": "sForceTest1"}]',

                                                 null);

     Test.setMock(HttpCalloutMock.class, mockResponse);
          
     Test.startTest();
     List<SocialPost> spt = TestUtil.creatTestSocialPost();
     insert spt[0];
     Test.stopTest();
       
        
     SocialPost sptTest = [select Id from SocialPost];
     Attachment attch = [select Id , ParentId from Attachment where ParentId=:sptTest.Id];
        
     System.assertEquals(sptTest.Id, attch.ParentId);

    }
    
    //This test method is to test the negative scenario , input:No attachment URL for SocialPost , output: creats attachments in SocialPost
  public static testmethod void testSocialAttachmentHandler2() {
  
     SingleRequestMock mockResponse = new SingleRequestMock(200,

                                                 'Complete',

                                                 '[{"Name": "sForceTest1"}]',

                                                 null);

     Test.setMock(HttpCalloutMock.class, mockResponse);
          
     Test.startTest();
     List<SocialPost> spt = TestUtil.creatTestSocialPost();
     insert spt[1];
     Test.stopTest();
       
        
     SocialPost sptTest = [select Id from SocialPost];
             
     System.assertEquals(0, [select Id , ParentId from Attachment where ParentId=:sptTest.Id].size());

    }

}